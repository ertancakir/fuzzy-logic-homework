/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmodev1;

import java.io.File;
import java.net.URISyntaxException;
import net.sourceforge.jFuzzyLogic.FIS;

/**
 *
 * @author ertan
 */
public class Celik {
    private FIS fis;
    private double sicaklik;
    private double su_bekletme_suresi;

    public Celik(double sicaklik, double su_bekletme_suresi) throws URISyntaxException {
        this.sicaklik = sicaklik;
        this.su_bekletme_suresi = su_bekletme_suresi;
        File dosya = new File(getClass().getResource("camasirMakinesi.fcl").toURI());
        fis = FIS.load(dosya.getPath(), true);
        
        fis.setVariable("soguma_hizi", sicaklik);
        fis.setVariable("su_bekletilme_suresi", su_bekletme_suresi);
        
        fis.evaluate();
    }

    @Override
    public String toString() {
        return "Çelik Sertliği : " + Math.round(fis.getVariable("yikama_suresi").getValue());
    }
    
}
